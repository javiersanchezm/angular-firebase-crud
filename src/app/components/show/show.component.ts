import { Component, OnInit } from '@angular/core';

//Importamos el modelo
import { Post } from 'src/app/post.model';
//Importamos el servicio
import { PostService } from 'src/app/post.service';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.sass']
})
export class ShowComponent implements OnInit {

  Posts!: Post[];

  constructor( private postSevice: PostService) { }

  ngOnInit(): void {
    this.postSevice.getPosts().subscribe( (resp) => {
      this.Posts = resp.map( (e)=>{
        return{
          id: e.payload.doc.id,
          ...(e.payload.doc.data() as Post)
        };
      });
    });
  }

  deleteRow = ( post:Post ) => {
    console.log(post.id, this.postSevice.deletePost( post ));
  };

}
