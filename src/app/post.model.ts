export class Post {
  id:string = '';
  author:string = '';
  content:string = '';
  title:string = '';
}
